#ifndef FONCTIONS_H
#define FONCTIONS_H

#include <opencv2/opencv.hpp>
#include <string>
#include <vector>
#include <stdio.h>
#include <RaspiCamCV.h>

using namespace cv;

#define MAX_PERSON 5
#define LARGEUR_PERSON_MIN 25
#define SCAN_STEP 2
#define SEUIL_VAL 90

typedef struct
{
	int xposition;
	int yposition;
	int distance;
	int largeur;

}Personne;

typedef struct
{
	Personne persons[MAX_PERSON];
	int nb_person_detectee;

}Scene;

Scene scanner_scene(const Mat& image);
void vertical_scanner(const Mat& image, int &person_counter, int largeur_de_person[]);

#endif
