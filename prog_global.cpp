#include "prog_glo.h"
#include "RaspiCamCV.h"


int main(int argc, char* argv[]){
	Scene scene_actuelle;
	
	//lancement camera
	RaspiCamCvCapture* capture = raspiCamCvCreateCameraCapture(0);
	
	Mat fond, img_actuelle;
	
	//ajouter buzzer ou led pour spécifier à l'opérateur de dégager
	//sleep(10);//attente qu'il dégage

	//on lance la photo du fond
	IplImage* image = cvCloneImage(raspiCamCvQueryFrame(capture));
	fond = cvarrToMat(image);
	
	imwrite("fond.jpg", fond);
	

	//attente avant de commencer l'algo ?
	clock_t clk;
	
	while(1){
		
		//clk=clock();

		image = raspiCamCvQueryFrame(capture);
		img_actuelle = cvarrToMat(image);
		
//		imwrite("fond_prog.jpg", fond);//devenue img actuelle
//		imwrite("actu_prog.jpg", img_actuelle);
		
		scene_actuelle=traitement_leong(fond, img_actuelle);
		
		clk=clk-clock();
		
		cout << "le temps est : " << ((float)clk)/CLOCKS_PER_SEC << endl;

		//sleep(1);//attente entre chaque itération ?
	}

	return 0;
}

