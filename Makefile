CC=g++
CFLAGS=-W -Wall -Wextra -ansi -pedantic -O3 -D_REENTRANT
LDFLAGS=-L /usr/lib/libuserland -lopencv_core -lopencv_imgproc -lopencv_highgui -lraspicamcv -lmmal_core -lmmal -l mmal_util -lvcos -lbcm_host
EXEC=prog-glo-bet
SRC= $(wildcard *.cpp)
OBJ= $(SRC:.cpp=.o)

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

#main.o: hello.h
prog_global.o: fonctions.h prog_glo.h

fonctions.o: fonctions.h prog_glo.h

%.o: %.cpp
	$(CC) -o $@ -c $< $(CFLAGS) -I /usr/include/opencv2

.PHONY: clean mrproper

clean:
		rm -rf *.o

mrproper: clean
		rm -rf $(EXEC)



#g++ -O3 -g -Wall -Wextra main.cpp -o main -I /usr/include/opencv2 -L /usr/lib -lopencv_core -lopencv_highgui
