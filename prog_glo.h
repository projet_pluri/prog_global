#pragma once

#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>
#include <pthread.h>
#include <ctime>

#include "fonctions.h"


using namespace cv;
using namespace std;

Scene traitement_leong(const Mat &image_fond, const Mat &image_courant);
