#include <opencv2/opencv.hpp>
#include <string>
#include <vector>

#include "fonctions.h"
#include "prog_glo.h"

using namespace cv;



Scene traitement_leong(const Mat &image_fond, const Mat &image_courant){
	Mat result;
	Scene scene;
	
	imwrite("actu_trait.jpg", image_courant);
	imwrite("fond_trait.jpg", image_fond);

	absdiff(image_fond, image_courant, result);

	imwrite("diff.jpg", result);
	threshold(result, result, SEUIL_VAL, 255, CV_THRESH_BINARY);
	
	imwrite("thresh.jpg", result);

	scene=scanner_scene(result);

	printf("le nombre de personnes detectees: %d\n", scene.nb_person_detectee);

	for(int i=0;i<scene.nb_person_detectee;i++){
		printf("la largeur de la personne %d : %d\n", i+1, scene.persons[i].largeur);
	}

	return scene;
}


//param[in] const Mat& image : doit etre seuiller avant
Scene scanner_scene(const Mat& image)
{
	Scene scene;
	int person_counter = 0;
	int largeur_de_person[MAX_PERSON];
	int i;

	vertical_scanner(image, person_counter, largeur_de_person);

	scene.nb_person_detectee = person_counter;

	for(i=0; i<person_counter;i++)
	{
		scene.persons[i].largeur = largeur_de_person[i];
	}
	return scene;
}



//scanneur sens vertical.
void vertical_scanner(const Mat& image, int &person_counter, int largeur_de_person[])
{

	int cumuleur = 0;
	int flag_enter = 0;

	int i, j;

	for(i = 0; i < image.cols; i+=SCAN_STEP)
	{
		j = 0;

		while( ( j < image.rows) && ( static_cast<int>(image.at<uchar>( j , i ) ) == 0 ))
		{
			j+=SCAN_STEP;
		}
		if(j != image.rows)
		{
			if(flag_enter ==0)
			{
				flag_enter = 1;
			}
			cumuleur+=SCAN_STEP;
		}
		else
		{
			if(flag_enter == 1)
			{
				if(cumuleur>LARGEUR_PERSON_MIN)
				{
					largeur_de_person[person_counter] = cumuleur;
					person_counter++;
				}
				flag_enter = 0;
				cumuleur = 0;
			}
		}
	}
	if(flag_enter == 1)												//si la boucle quitte mais on n'a toujours pas fini de mesurer la largeur d'une personne
	{
		if(cumuleur>LARGEUR_PERSON_MIN)
		{
			largeur_de_person[person_counter] = cumuleur;
			person_counter++;
		}
		flag_enter = 0;
		cumuleur = 0;
	}
}
